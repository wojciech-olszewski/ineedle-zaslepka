<?php include('site/header.php')?>
<body>

	<div class="main-area center-text">
		<div class="display-table">
			<div class="display-table-cell">
				<div class="container">
					<div class="col-md-12">
						<a href="index.php"><img src="03-comming-soon/logo.png" style="width:auto;margin-bottom:30px;" class="img-fluid"/></a>

						<?php
							$kodankietowanego = $_SESSION['zapisanykod'];
?>

<?php

		$email_from = "automat@ineedle.pl";
    $email_to = "a.toczynska@ineedle.pl";
    $email_subject = $_SESSION['address']." wypełnił ankietę na iNEEDle.pl";

    function died($error) {
        die();
    }


		$ans1 = $_SESSION['1answer']; //
    $tak1 = $_SESSION['1tak']; //
    $ans2 = $_SESSION['2answer']; //
    $tak2 = $_SESSION['2tak']; //
    $ans3 = $_SESSION['3answer']; //

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

    $email_message = $_SESSION['address']." wypełnił ankietę na stronie iNEEDle.pl. Poniżej znajdują się jego odpowiedzi.\n\n";


    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }

		$kod = $SESSION['kod'];
    $email_message .= "Pytanie pierwsze: ".clean_string($ans1).clean_string($tak1)."\n";
    $email_message .= "Pytanie drugie: ".clean_string($ans2).clean_string($tak2)."\n";
    $email_message .= "Pytanie trzecie: ".clean_string($ans3)."\n";
		$email_message .= "Kod zniżkowy, który otrzymał ankietowany: ".clean_string($kodankietowanego)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);
?>

<h4>Dziękujemy za wyrażenie swojej opinii.<br /><br />Na wskazany adres e-mail został wysłany kod rabatowy do naszego sklepu.</h4>
<?php session_destroy(); ?>

					</div>
				</div>

			</div><!-- display-table -->
		</div><!-- display-table-cell -->
	</div><!-- main-area -->

<?php include('site/footer.php') ?>
