<?php include('site/header.php')?>
<body>

	<div class="main-area center-text">
		<div class="display-table">
			<div class="display-table-cell">
				<div class="container">
					<div class="col-md-12">
						<a href="index.php"><img src="03-comming-soon/logo.png" style="width:auto;margin-bottom:30px;" class="img-fluid"/></a>
						</a/>

						<h6 id="pytanie">Jeśli przeszyłaś już coś z naszym wykrojem i chcesz otrzymać 20% rabatu na zakup jednego z naszych wykrojów, to będzie nam miło, jeśli podzielisz się z nami swoją opinią i odpowiesz na trzy krótkie pytania:</h6>
						<form action="" method="post">
						  <div class="form-group">
						    <label for="exampleInputEmail1"><h4>Czy nasz wykrój spełnił Twoje oczekiwania?</h4></label>
								<div class="row">
									<div class="col-md-4">
									</div>
									<div class="col-md-4">
										<button type="submit" type="submit" class="btn btn-success" name="SubmitTak">TAK</button>
										<button type="button" class="btn btn-danger" name="1nie" id="nie">NIE</button>
									</div>
								</div>

								<textarea class="form-control animated fadeIn delay-2s" id="FormTextarea" rows="3" placeholder="Napisz, co powinniśmy zmienić" name="answer"></textarea>

						  </div>
							<hr />
						  <button type="submit" class="btn btn-primary" type="submit" name="Submit" id="dalej" disabled='disabled'>DALEJ</button>
						</form>
						<?php

if (isset($_POST['Submit'])) {
 $_SESSION['1answer'] = $_POST['answer'];
 header('Location: q2.php');
 }

if (isset($_POST['SubmitTak'])) {
	$_SESSION['1tak'] = "Ankietowany odpowiedział TAK";
	header('Location: q2.php');
}

?>
					</div>
				</div>
			</div><!-- display-table -->
		</div><!-- display-table-cell -->
	</div><!-- main-area -->

<?php include('site/footer.php') ?>
