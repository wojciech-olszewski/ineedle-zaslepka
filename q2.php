<?php include('site/header.php')?>
<body>

	<div class="main-area center-text">
		<div class="display-table">
			<div class="display-table-cell">
				<div class="container">
					<div class="col-md-12">
						<a href="index.php"><img src="03-comming-soon/logo.png" style="width:auto;margin-bottom:30px;" class="img-fluid"/></a>
						<form action="" method="post">
						  <div class="form-group">
						    <label for="exampleInputEmail1"><h4>Czy nasz wykrój ułatwił Ci szycie?</h4></label>
								<div class="row">
									<div class="col-md-4">
									</div>
									<div class="col-md-4">
										<button type="submit" type="submit" class="btn btn-success" name="SubmitTak">TAK</button>
										<button type="button" class="btn btn-danger" name="2nie" id="nie">NIE</button>
									</div>
								</div>

								<textarea class="form-control" id="FormTextarea" rows="3" placeholder="Napisz, co powinniśmy zmienić" name="answer"></textarea>

						  </div>
							<hr />
						  <button type="submit" class="btn btn-primary" type="submit" name="Submit" id="dalej" disabled='disabled'>DALEJ</button>
						</form>
						<?php

if (isset($_POST['Submit'])) {
 $_SESSION['2answer'] = $_POST['answer'];
 header('Location: q3.php');
 }

if (isset($_POST['SubmitTak'])) {
	$_SESSION['2tak'] = "Ankietowany odpowiedział TAK";
	header('Location: q3.php');
}

?>
					</div>
				</div>

			</div><!-- display-table -->
		</div><!-- display-table-cell -->
	</div><!-- main-area -->

<?php include('site/footer.php') ?>
